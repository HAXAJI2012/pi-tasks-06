#include "Dekanat.h"
#include "Group.h"
#include "Student.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>

using namespace std;

Dekanat::Dekanat()
{
	numOfStuds = 0;
	numOfGroups = 0;
}

void Dekanat::studCreate_file(string fileName)
{
	ifstream file;
	string currFio = "";
	string currId = "";
	file.open(fileName); //� main()
	if (file.is_open())
	{
		while (!file.eof())
		{
			getline(file, currId);
			getline(file, currFio);
			studs[numOfStuds] = new Student(atoi(currId.c_str()), currFio);
			numOfStuds++;
		}
	}
	file.close();
}

void Dekanat::addGR(Group* _GR)
{
	if (numOfGroups < maxGRnum)
	{
		groups[numOfGroups] = _GR;
		numOfGroups++;
	}
	else
		cout << "Groups are full!" << endl;
}

void Dekanat::groupsCreate_file(string fileName)
{
	ifstream file;
	string currTitle = "";
	file.open(fileName);
	if (file.is_open())
	{
		while (!file.eof())
		{
			getline(file, currTitle);
			addGR(new Group(currTitle));
		}
	}
	file.close();
}

void Dekanat::randMarks(int randM)
{
	srand((int)time(NULL));
	for (int i = 0; i < numOfStuds; i++)
		for (int j = 0; j < randM; j++)
			studs[i]->addMark(rand() % 5 + 1);
}

void Dekanat::bestStud(double Bmark)
{
	cout << "LIST OF BEST STUDENTS!" << endl;
	for (int i = 0; i < numOfStuds; i++)
		if (Bmark >= studs[i]->averageMark())
			cout << studs[i]->getFIO() << " with average mark: " << studs[i]->averageMark() << endl;
	cout << "\\\\\\\\\////////" << endl;
}

void Dekanat::bestGroup(double Bmark)
{
	cout << "LIST OF BEST GROUPS!" << endl;
	for (int i = 0; i < numOfGroups; i++)
		if (Bmark >= groups[i]->averageMark())
			cout << groups[i]->getTitle() << " with average mark: " << groups[i]->averageMark() << endl;
	cout << "\\\\\\\\\////////" << endl;
}

void Dekanat::grTOgr(Student* _student, Group* _newGroup)
{
	_student->getGR()->minusStud(_student->getID());
	_newGroup->addStudent(_student);
}

void Dekanat::kickStud(double lowAvgMark)
{
	int i = 0; //� ������ ������ while ������� for
	while (i < numOfStuds)
	{
		if (studs[i]->averageMark() < lowAvgMark)
		{
			studs[i]->getGR()->minusStud(studs[i]->getFIO());
			delete studs[i];
			numOfStuds--;
			studs[i] = studs[numOfStuds];
		}
		else
			i++;
	}
}

void Dekanat::writeStud_file(string fileName)
{
	ofstream file;
	file.open(fileName);
	if (file.is_open())
		for (int i = 0; i < numOfStuds; i++)
		{
			file << studs[i]->getID() << "\t" << studs[i]->getFIO() << "\t" << studs[i]->getGR() << endl;
			studs[i]->markInfo(file);
			file << "\\\\\\||||||" << endl;
		}
}

void Dekanat::writeGroup_file(string fileName)
{
	ofstream file;
	file.open(fileName);
	if (file.is_open())
		for (int i = 0; i < numOfGroups; i++)
			file << groups[i]->getTitle() << endl;
}

void Dekanat::setHeads_rand()
{
	for (int i = 0; i < numOfGroups; i++)
		groups[i]->setRandHead();
}

void Dekanat::printInfo()
{
	for (int i = 0; i < numOfGroups; i++)
		groups[i]->grInfo();
}

void Dekanat::addST(Student* _ST)
{
	if (numOfStuds < maxStudents)
	{
		studs[numOfStuds] = _ST;
		numOfStuds++;
	}
	else
		cout << "Sorry, but the group is full!" << endl;
}

void Dekanat::raspred()
{
	srand((int)time(NULL));
	for (int i = 0; i < numOfStuds; i++)
	{
		studs[i]->setGR(groups[rand() % numOfGroups]);
		studs[i]->getGR()->addStudent(studs[i]);
	}
}

Dekanat::~Dekanat()
{
	for (int i = 0; i < numOfStuds; i++)
		delete studs[i];
	for (int i = 0; i < numOfGroups; i++)
		delete groups[i];
}