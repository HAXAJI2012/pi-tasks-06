#pragma once
#include <iostream>
#include <string>
#include "Group.h"

using namespace std;

class Group;

class Student
{
private:
	int id;
	string fio;
	Group* studGR;
	int *marks;
	int amountOfMarks;
public:
	Student(void);
	Student(int _id, string _fio);
	Student(int _id, string _fio, Group* _studGR);
	void setID(int _id);
	int getID();
	void setFIO(string _fio);
	string getFIO();
	void setGR(Group *_studGR);
	Group* getGR();
	void addMark(int mark);
	double averageMark();
	void studInfo();
	void markInfo(ostream& stream);
	~Student(void);
};