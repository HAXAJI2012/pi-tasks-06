#pragma once
#include <string>
#include "Student.h"

using namespace std;

class Student;
class Group
{
private:
	string title;
	int num;
	Student* head;
	Student** studArr;
public:
	Group(void);
	Group(string _title);
	Group(string _title, Student* _head);

	void setTitle(string _title);
	string getTitle();
	void addStudent(Student* _student);
	int getStudNum();
	void setHead(Student* _head);
	Student* getHead();
	Student* searchST(int _id);
	Student* searchST(string _fio);
	void minusStud(int _id);
	void minusStud(string _fio);
	double averageMark();
	void setRandHead(); //������������ � dekanat
	void grInfo();

	~Group();
};