#pragma once
#include <iostream>
#include <fstream>
#include "Student.h"
#include "Group.h"

const int maxGRnum = 10;
const int maxStudents = 200;

class Dekanat
{
private:
	Student* studs[maxStudents];
	Group* groups[maxGRnum];
	int numOfStuds;
	int numOfGroups;
public:
	Dekanat();

	void studCreate_file(string fileName);
	void groupsCreate_file(string fileName);
	void randMarks(int randM);
	void bestStud(double Bmark);
	void bestGroup(double Bmark);
	void grTOgr(Student* _student, Group* _newGroup);
	void kickStud(double lowAvgMark);
	void writeStud_file(string fileName);
	void writeGroup_file(string fileName);
	void setHeads_rand();
	void printInfo();
	void addGR(Group* _GR);
	void addST(Student* _ST);
	void raspred();

	~Dekanat();
};