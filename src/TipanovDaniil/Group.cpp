#include "Group.h"
#include <string>
#include <stdlib.h>
#include <time.h>

const int grMAX = 20;

Group::Group()
{
	title = "";
	num = 0;
	head = nullptr;
	studArr = new Student*[grMAX];
}

Group::Group(string _title)
{
	title = _title;
	head = nullptr;
	studArr = new Student*[grMAX];
	num = 0;
}

Group::Group(string _title, Student* _head)
{
	title = _title;
	head = _head;
	studArr = new Student*[grMAX];
	studArr[0] = head;
	num = 1;
}

void Group::setTitle(string _title) { title = _title; }
string Group::getTitle() { return title; }
void Group::addStudent(Student* _student)
{
	if (num < grMAX)
	{
		num++;
		studArr[num] = _student;
		_student->setGR(this);
	}
	else
		cout << "Sorry, but this group is full!" << endl;
}

int Group::getStudNum() { return num; }

void Group::setHead(Student* _head) { head = _head; }
Student* Group::getHead() { return head; }

Student* Group::searchST(int _id)
{
	for (int i = 0; i < num; i++)
		if (studArr[i]->getID() == _id)
			return studArr[i];
	cout << "Student wasn't found: " << _id << endl;
	return nullptr;
}

Student* Group::searchST(string _fio)
{
	for (int i = 0; i < num; i++)
		if (studArr[i]->getFIO() == _fio)
			return studArr[i];
	cout << "Student wasn't found: " << _fio << endl;
	return nullptr;
}

void Group::minusStud(int _id)
{
	for(int i = 0; i < num; i++)
		if (studArr[i]->getID() == _id)
		{
			if (studArr[i] == head) //���� �������� - ���������� �������� head
				head = nullptr;
			if (studArr[i] != studArr[num - 1]) //������ ������ �� ������� ������
				studArr[i] = studArr[num - 1];
			studArr[num - 1] = nullptr;
			num--;
			break;
		}
}

void Group::minusStud(string _fio)
{
	for (int i = 0; i < num; i++)
		if (studArr[i]->getFIO() == _fio)
		{
			if (studArr[i] == head)
				head = nullptr;
			if (studArr[i] != studArr[num - 1])
				studArr[i] = studArr[num - 1];
			studArr[num - 1] = nullptr;
			num--;
			break;
		}
}

double Group::averageMark()
{
	if (num == 0)
		cout << "There is no students in this group." << endl;
	else
	{
		double avg = 0;
		for (int i = 0; i < num; num++)
			avg += studArr[i]->averageMark();
		avg /= num;
		return avg;
	}
}

void Group::setRandHead()
{
	srand((int)time(NULL));
	if (num != 0)
		head = studArr[rand() % num];
}

void Group::grInfo()
{
	cout << "Group: " << title << endl;
	cout << "Number of students: " << num << endl;
	if (head == nullptr)
		cout << "There is no head in this group!" << endl;
	else
		cout << "Head: " << head->getFIO() << endl;
	cout << "STUDENTS" << endl;
	for (int i = 0; i < num; i++)
	{
		studArr[i]->studInfo();
		cout << "\\\\\\" << endl;
	}
}

Group::~Group()
{
	title = "";
	num = 0;
	delete head;
	delete[] studArr;
}