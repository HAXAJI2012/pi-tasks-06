#include "Student.h"
#include "Group.h"
#include "Dekanat.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
	Dekanat dean;
	dean.groupsCreate_file("GroupsList.txt");
	dean.studCreate_file("StudList.txt");
	dean.randMarks(13);
	//??
	dean.setHeads_rand();
	dean.printInfo();
	dean.kickStud(3);
	dean.printInfo(); //after kick!
	dean.bestStud(4);
	dean.bestGroup(4);
	dean.writeStud_file("StudListAFTERkick.txt");
	dean.writeGroup_file("GroupsListAFTERkick.txt");
	return 0;
}