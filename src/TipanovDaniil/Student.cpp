#include "Student.h"
#include "Group.h"
#include <iostream>

Student::Student(void)
{
	id = 0;
	fio = "";
	studGR = nullptr;
	marks = nullptr;
	amountOfMarks = 0;
}

Student::Student(int _id, string _fio) : id(_id), fio(_fio), studGR(nullptr), marks(nullptr), amountOfMarks(0) {};

Student::Student(int _id, string _fio, Group* _studGR) : id(_id), fio(_fio), studGR(_studGR), marks(nullptr), amountOfMarks(0) {};

void Student::setID(int _id) { id = _id; }
int Student::getID() { return id; }
void Student::setFIO(string _fio) { fio = _fio; }
string Student::getFIO() { return fio; }
void Student::setGR(Group* _studGR) { studGR = _studGR; }
Group* Student::getGR() { return studGR; }

void Student::addMark(int mark)
{
	if (amountOfMarks == 0)
	{
		marks = new int[1];
		marks[amountOfMarks] = mark;
	}
	else
	{
		int* temp = new int[amountOfMarks + 1];
		for (int i = 0; i < amountOfMarks; i++)
			temp[i] = marks[i];
		temp[amountOfMarks] = mark;
		delete[] marks;
		marks = temp;
	}
}

double Student::averageMark()
{
	if (amountOfMarks == 0)
		cout << "No marks!" << endl;
	else
	{
		int avg = 0;
		for (int i = 0; i < amountOfMarks; i++)
			avg += marks[i];
		avg /= amountOfMarks;
		return avg;
	}
}

void Student::studInfo()
{
	cout << "ID: " << id << endl;
	cout << "FIO: " << fio << endl;
	cout << "Average mark: " << averageMark() << endl;
}

void Student::markInfo(ostream& stream)
{
	for (int i = 0; i < amountOfMarks; i++)
		stream << marks[i] << " ";
	stream << "\\\\\|||||" << endl;
}

Student::~Student(void)
{
	id = 0;
	fio = "";
	delete[] marks;
	delete studGR;
}