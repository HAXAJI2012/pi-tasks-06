#include <string>

using namespace std;

class Group
{
private:
	string Title;
	Student ** Students;
	int Num;
	Student * Head;
public:
	Group(string Title) 
	{
		this->Title = Title;
		Num = 0;
		Students = new Student * [Num];
	}
	void addStudent(Student * st);
	void setHead(Student * st);
	Student * getHead();
	Student * searchStud(string FIO);
	Student * searchStud(int ID);
	double getAverageRating();
	void excludeStud(Student * st);
	string getTitle();
	int getNum();
	Student * getStud(int Index);
	~Group()
	{
		delete[]Students;
	}
};