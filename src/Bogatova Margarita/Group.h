#pragma once
#include <string>
#include <iostream>
#include "Student.h"
class Group
{
private:
	int Title;
	Student **Staff;
	Student *head;
	int size;
public:
	Group();
	Group(int name);
	int getTitle();
	void addStudent(Student *S);
	Student* findStudent(int id);
	void RemoveStudent(int id);
	void setHead();
	Student* getHead();
	float getAvMark();
};
