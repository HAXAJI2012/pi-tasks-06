#define _CRT_SECURE_NO_WARNINGS
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include "Group.h"
using namespace std;
class Dekanat {
	vector<Group*> groups;
	vector<Student*> students;
public:
	Dekanat();
	void LoadGroups();
	void LoadStud();
	Student* FindStud(int id);
	Group* FindGr(string title);
	void RemoveStudGr(Student* stud);
	void AddStudGr(Student* stud, Group* gr);
	void TransferStud(int id, string title);
	void AddStud(Student* new_stud, string title);
	void RemoveStud(int id);
	void AddMark(int id, int mark);
	void AddHead(string title, int id);
	void RandomMarks(int size);
	Group* FindBestGroup();
	Student* FindBestStudent();
	void WriteStudents();
	void WriteGroups();
	void Scenario();
	~Dekanat();
};

